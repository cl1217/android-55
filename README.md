# README #

Photo App created by Charles Li & Max Sun

### Features ###

* Creating your own photo albums
* Storing photos in your albums
* Tagging your photos by people and location
* Searching your photos by tags
* Edit, Deleting, Moving Photos
* Adding & Removing Photo tags
